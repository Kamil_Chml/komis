package pl.sda;

import javax.persistence.*;

/**
 * Created by x on 2017-05-05.
 */
@Entity
@Table(name="Seller")
public class Seller {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column
    private String nazwisko;
    @Column
    private String imie;
    @Column
    private String firma;
    @Column
    private String nip;
    @Column
    private String pesel;

    public Seller(String nazwisko, String imie, String nip, String pesel, String firma) {
        this.nazwisko = nazwisko;
        this.imie = imie;
        this.nip = nip;
        this.pesel = pesel;
        this.firma = firma;
    }

    public Seller() {
    }

    public String getFirma() {
        return firma;
    }

    public void setFirma(String firma) {
        this.firma = firma;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }
}
