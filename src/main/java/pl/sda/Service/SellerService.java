package pl.sda.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.Cars;
import pl.sda.DTO.SellerDto;
import pl.sda.Repository.CarRepository;
import pl.sda.Repository.SellerRepository;
import pl.sda.Seller;

/**
 * Created by x on 2017-05-08.
 */
@Service
public class SellerService {

    @Autowired
    private SellerRepository sellerRepository;

    @Autowired
    private CarRepository carRepository;

    public void saveSellerToDataBase(SellerDto sellerDto){
        Seller seller = new Seller();

        Integer carId = sellerDto.getCar();

        seller.setImie(sellerDto.getImie());
        seller.setNazwisko(sellerDto.getNazwisko());
        seller.setFirma(sellerDto.getFirma());
        seller.setPesel(sellerDto.getPesel());
        seller.setNip(sellerDto.getNip());
        Seller savedSeller = sellerRepository.save(seller);
        //todo: wyciagnij car z carRepository po id ze zmiennej car

        Cars carSavedOnLastScreen = carRepository.findOne(carId);

        //todo: dodaj save w car wyciagnietym z carRepository
        carSavedOnLastScreen.setSeller(savedSeller);


        //todo: zapisz car poprzez carRepository
        carRepository.save(carSavedOnLastScreen);
    }


}
