package pl.sda.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.Cars;
import pl.sda.DTO.CarDto;
import pl.sda.Exceptions.PriceException;
import pl.sda.Repository.CarRepository;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by x on 2017-05-04.
 */
@Service
public class CarService {


    @Autowired
    private CarRepository carRepository;

    public List<Cars> getCarList() {

        List<Cars> notSoldCars = carRepository.findAll();
        List<Cars> collect = notSoldCars.stream().filter(n -> n.getSprzedany().equals(0)).collect(Collectors.toList());
        return collect;
    }


    public List<Cars> setSoldCar(Integer id) {
        Cars one = carRepository.findOne(id);
        one.setSprzedany(1);
        carRepository.save(one);
        return null;

    }

    public Cars viewThisCar(Integer id) {
        Cars one = carRepository.findOne(id);
        one.getId();
        one.getMarka();
        one.getModel();
        one.getVin();
        one.getMoc();
        one.getNr_oc();
        one.getNr_dowodu_rej();
        one.getPaliwo();
        one.getPrzebieg();
        one.getRok();
        one.getSkrzynia_biegow();
        one.getSprzedany();
        one.getSeller();


        return one;
    }

    public List<Cars> showSoldCars() {

        List<Cars> all = carRepository.findAll();
        List<Cars> collect = all.stream().filter(s -> s.getSprzedany().equals(1)).collect(Collectors.toList());

        return collect;

    }

    public Integer showAmountOfSoldCars(){
        int policz = 0;
        List<Cars> all = carRepository.findAll();
        List<Cars> collect = all.stream().filter(s -> s.getSprzedany().equals(1)).collect(Collectors.toList());




        for (int i = 0; i <=collect.size() ; i++) {
            policz = i;
        }
        return policz;



    }

    public Long showValueOfSoldCars () {
        Long suma = new Long(0);
        List<Cars> all = carRepository.findAll();
        List<Cars> collect = all.stream().filter(s -> s.getSprzedany().equals(1)).collect(Collectors.toList());

        for (Cars cars : collect) {
            suma += cars.getCena();
        }
        return suma;
    }

    public double profitForCompany(){

        Long suma2 = new Long(0);
        Long suma = new Long(0);

        suma2 = new Long(0);
        List<Cars> all = carRepository.findAll();
        List<Cars> collect = all.stream().filter(s -> s.getSprzedany().equals(1)).collect(Collectors.toList());

        for (Cars cars : collect) {
            suma += cars.getCena();

        }
        return (Long) suma*0.2;
    }

    public double profitForClients() {
        Long suma2 = new Long(0);
        Long suma = new Long(0);

        suma2 = new Long(0);
        List<Cars> all = carRepository.findAll();
        List<Cars> collect = all.stream().filter(s -> s.getSprzedany().equals(1)).collect(Collectors.toList());

        for (Cars cars : collect) {
            suma += cars.getCena();

        }
        return (Long)suma*0.8;
    }


    public Integer saveCarToDataBase(CarDto carDto) {

        Cars car = new Cars();
        car.setMarka(carDto.getMarka());

        car.setModel(carDto.getModel());

        Integer cena = carDto.getCena();
        if (cena >= 5000) {
            car.setCena(carDto.getCena());
        } else {
            throw new PriceException("cena musi być >= 5000 PLN");
        }
        car.setPrzebieg(carDto.getPrzebieg());
        car.setRok(carDto.getRok());
        car.setMoc(carDto.getMoc());
        car.setNr_dowodu_rej(carDto.getNr_dowodu_rej());
        car.setPrzebieg(carDto.getPrzebieg());
        car.setNr_oc(carDto.getNr_oc());
        car.setSkrzynia_biegow(carDto.getSkrzynia_biegow());
        car.setPaliwo(carDto.getPaliwo());

        car.setVin(carDto.getVin());
        car.setSprzedany(0);
        return carRepository.save(car).getId();


    }


}






