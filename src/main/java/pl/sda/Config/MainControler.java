package pl.sda.Config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.sda.DTO.CarDto;
import pl.sda.DTO.SellerDto;
import pl.sda.Exceptions.PriceException;
import pl.sda.Service.CarService;
import pl.sda.Service.SellerService;
import pl.sda.VinValidator;

import javax.validation.Valid;

/**
 * Created by x on 2017-05-04.
 */
@Controller
@RequestMapping
public class MainControler {

    @Autowired
    private CarService carService;
    @Autowired
    private SellerService sellerService;
    @Autowired
    private VinValidator vinValidator;

    private final Logger log = LoggerFactory.getLogger(MainControler.class);

    @GetMapping("/logowanie")
    public ModelAndView login(ModelMap modelMap){
        return new ModelAndView("/login",modelMap);
    }

    @RequestMapping({"/", "/home"})
    public ModelAndView showAllCars(ModelMap modelMap) {


        modelMap.addAttribute("rows", carService.getCarList());

        return new ModelAndView("index");
    }


    @GetMapping("/danePojazdu")
    public ModelAndView getCarData(ModelMap modelMap) {
        modelMap.addAttribute("carDto", new CarDto());
        return new ModelAndView("CarsInput", modelMap);
    }


//    @GetMapping("/daneSellera")
//    public ModelAndView getSellerData (ModelMap modelMap){
//        modelMap.addAttribute("sellerDto", new SellerDto());
//        return new ModelAndView("SellerInput",modelMap);
//    }

    @PostMapping("/daneSellera")
    public ModelAndView submit(@ModelAttribute(name = "sellerDto") SellerDto sellerDto, ModelMap modelMap) {
        sellerService.saveSellerToDataBase(sellerDto);
        modelMap.addAttribute("rows", carService.getCarList());
        return new ModelAndView("SuccesCarAdded", modelMap);
    }


    @GetMapping("details/{id}")
    public ModelAndView getCarDetails(@PathVariable("id") Integer id, ModelMap modelMap) {


        modelMap.addAttribute("rows", carService.viewThisCar(id));


        return new ModelAndView("CarDetails", modelMap);
    }


    @PostMapping("/danePojazdu")
    public ModelAndView submit(@Valid @ModelAttribute(name = "carDto") CarDto carDto, BindingResult bindingResult, ModelMap modelMap) {
        vinValidator.validate(carDto, bindingResult);
        Integer carToDataBase;
        if (bindingResult.hasErrors()) {
            modelMap.addAttribute(carDto);
            return new ModelAndView("CarsInput", modelMap);
        } else {
            carToDataBase = carService.saveCarToDataBase(carDto);
        }
        modelMap.addAttribute("sellerDto", new SellerDto());

        ModelAndView modelAndView = new ModelAndView("SellerInput", modelMap);
        modelAndView.addObject("carId", carToDataBase);
        return modelAndView;


    }


    @GetMapping("/sold/{id}")
    public ModelAndView sold(@PathVariable("id") Integer id, ModelMap modelMap) {

        carService.setSoldCar(id);
        modelMap.addAttribute("rows", carService.showSoldCars());

        return new ModelAndView("SuccessCarSold");
    }


    @GetMapping("/Archiv")
    public ModelAndView archiv(ModelMap modelMap) {
        modelMap.addAttribute("rows", carService.showSoldCars());
        return new ModelAndView("Archiv");
    }

    @GetMapping("/RaportZysku")
    public ModelAndView zysk(ModelMap modelMap){

        return new ModelAndView("ProfitRaport", modelMap);
    }


    @GetMapping("/RaportSprzedazy")
    public ModelAndView sales(ModelMap modelMap){

        modelMap.addAttribute("sold",carService.showSoldCars());
        modelMap.addAttribute("iloscAut", carService.showAmountOfSoldCars());
        modelMap.addAttribute("wartoscSprzedazy", carService.showValueOfSoldCars());
        modelMap.addAttribute("zyskFirmy",carService.profitForCompany());
        modelMap.addAttribute("zyskKlientow", carService.profitForClients());


       return new ModelAndView("SalesRaport", modelMap);
    }


    @ExceptionHandler(PriceException.class)
    public String metodaBleduCeny () {
        return "priceError";

}


}
