package pl.sda;

import javax.persistence.*;

/**
 * Created by x on 2017-05-04.
 */
@Entity
@Table ( name = "Cars")
public class Cars {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String marka;
    @Column
    private String model;
    @Column
    private Integer rok;
    @Column
    private String przebieg;
    @Column
    private Integer cena;
    @Column
    private String vin;
    @Column
    private String nr_oc;
    @Column
    private String nr_dowodu_rej;
    @Column
    private String paliwo;
    @Column
    private String moc;
    @Column
    private String skrzynia_biegow;

    @Column
    private Integer sprzedany;

    @ManyToOne
    @JoinColumn(name="owner_id")
    private Seller seller;

    public Seller getSeller() {
        return seller;
    }

    public void setSeller(Seller seller) {
        this.seller = seller;
    }

    public Cars() {
    }

    public Cars(String marka, String model, Integer rok, String przebieg, Integer cena,
                String vin, String nr_oc, String nr_dowodu_rej, String paliwo, String moc,
                String skrzynia_biegow, String ilosc_jazd_probnych, Integer sprzedany) {
        this.marka = marka;
        this.model = model;
        this.rok = rok;
        this.przebieg = przebieg;
        this.cena = cena;
        this.vin = vin;
        this.nr_oc = nr_oc;
        this.nr_dowodu_rej = nr_dowodu_rej;
        this.paliwo = paliwo;
        this.moc = moc;
        this.skrzynia_biegow = skrzynia_biegow;

        this.sprzedany = sprzedany;

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMarka() {
        return marka;
    }

    public void setMarka(String marka) {
        this.marka = marka;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getRok() {
        return rok;
    }

    public void setRok(Integer rok) {
        this.rok = rok;
    }

    public String getPrzebieg() {
        return przebieg;
    }

    public void setPrzebieg(String przebieg) {
        this.przebieg = przebieg;
    }

    public Integer getCena() {
        return cena;
    }

    public void setCena(Integer cena) {
        this.cena = cena;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getNr_oc() {
        return nr_oc;
    }

    public void setNr_oc(String nr_oc) {
        this.nr_oc = nr_oc;
    }

    public String getNr_dowodu_rej() {
        return nr_dowodu_rej;
    }

    public void setNr_dowodu_rej(String nr_dowodu_rej) {
        this.nr_dowodu_rej = nr_dowodu_rej;
    }

    public String getPaliwo() {
        return paliwo;
    }

    public void setPaliwo(String paliwo) {
        this.paliwo = paliwo;
    }

    public String getMoc() {
        return moc;
    }

    public void setMoc(String moc) {
        this.moc = moc;
    }

    public String getSkrzynia_biegow() {
        return skrzynia_biegow;
    }

    public void setSkrzynia_biegow(String skrzynia_biegow) {
        this.skrzynia_biegow = skrzynia_biegow;
    }


    public Integer getSprzedany() {
        return sprzedany;
    }

    public void setSprzedany(Integer sprzedany) {
        this.sprzedany = sprzedany;
    }
}
