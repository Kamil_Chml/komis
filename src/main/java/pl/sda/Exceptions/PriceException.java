package pl.sda.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by x on 2017-05-17.
 */
@ResponseStatus ( value = HttpStatus.FORBIDDEN, reason = "!!!!!!!!!!!!!!!!! NIE SPRZEDAJEMY SAMOCHODÓW ZA MNIEJ NIŻ 5000 ZŁ !!!!!!!!!!!!!!!!!!!!!!")
public class PriceException extends RuntimeException {
    public PriceException(String message) {
        super(message);
    }
}
