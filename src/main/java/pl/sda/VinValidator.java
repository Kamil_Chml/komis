package pl.sda;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import pl.sda.DTO.CarDto;
import pl.sda.Repository.CarRepository;

/**
 * Created by x on 2017-05-08.
 */
@Component
public class VinValidator implements Validator {

    @Autowired
    CarRepository carRepository;

    @Override
    public boolean supports(Class<?> aClass) {
        return false;
    }

    @Override
    public void validate(Object c, Errors errors) {

        CarDto carDto = (CarDto) c;

        carDto.getVin();
        Integer integer = carRepository.countCars(carDto.getVin());
        if(!integer.equals(0)){
            errors.rejectValue("vin","", "Podany VIN już istnieje!");

        }


    }
}
