package pl.sda.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.sda.Cars;

/**
 * Created by x on 2017-05-04.
 */
@Repository
public interface CarRepository extends JpaRepository <Cars, Integer> {

@Query("Select count (c.id) from Cars c where c.vin = :vin")
Integer countCars(@Param("vin")String vin);

}
