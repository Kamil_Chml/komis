package pl.sda.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.Seller;

/**
 * Created by x on 2017-05-08.
 */
@Repository
public interface SellerRepository extends JpaRepository <Seller, Integer>{
}
