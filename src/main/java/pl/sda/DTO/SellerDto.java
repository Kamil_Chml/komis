package pl.sda.DTO;

/**
 * Created by x on 2017-05-08.
 */
public class SellerDto {



    private String nazwisko;
    private Integer car;

    private String imie;

    private String firma;

    private String nip;

    private String pesel;

    public SellerDto() {
    }

    public Integer getCar() {
        return car;
    }

    public void setCar(Integer car) {
        this.car = car;
    }

    public SellerDto(String nazwisko, String imie, String firma, String nip, String pesel) {
        this.nazwisko = nazwisko;
        this.imie = imie;
        this.firma = firma;
        this.nip = nip;
        this.pesel = pesel;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getFirma() {
        return firma;
    }

    public void setFirma(String firma) {
        this.firma = firma;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }
}
