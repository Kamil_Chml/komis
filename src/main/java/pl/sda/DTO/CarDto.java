package pl.sda.DTO;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Created by x on 2017-05-05.
 */
public class CarDto {

    @NotEmpty(message = "podaj markę")
    private String marka;
    @NotEmpty(message = "podaj model")
    private String model;
    @NotNull(message = "podaj rok")
    private Integer rok;
    @NotEmpty(message = "podaj przbeieg")
    private String przebieg;
    @Min(value = 5000, message = "Cena musi być większa niż 5000 zł!!!")
    @NotNull(message = "podaj cenę")
    private Integer cena;
    @NotEmpty(message = "podaj vin")
    private String vin;
    @NotEmpty(message = "podaj numer ubezpieczenia OC")
    private String nr_oc;
    @NotEmpty(message = "podaj numer dowodu rejestracyjnego")
    private String nr_dowodu_rej;

    private String paliwo;
    @NotEmpty(message = "podaj moc silnika")
    private String moc;

    private String skrzynia_biegow;
    @NotNull(message = "podaj markę")
    private Integer sprzedany;

    public CarDto(String marka, String model, Integer rok, String przebieg, Integer cena,
                  String vin, String nr_oc, String nr_dowodu_rej, String paliwo,
                  String moc, String skrzynia_biegow, Integer sprzedany) {
        this.marka = marka;
        this.model = model;
        this.rok = rok;
        this.przebieg = przebieg;
        this.cena = cena;
        this.vin = vin;
        this.nr_oc = nr_oc;
        this.nr_dowodu_rej = nr_dowodu_rej;
        this.paliwo = paliwo;
        this.moc = moc;
        this.skrzynia_biegow = skrzynia_biegow;
        this.sprzedany = sprzedany;
    }

    public CarDto() {
    }

    public Integer getSprzedany() {
        return sprzedany;
    }

    public void setSprzedany(Integer sprzedany) {
        this.sprzedany = sprzedany;
    }

    public String getMarka() {
        return marka;
    }

    public void setMarka(String marka) {
        this.marka = marka;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getRok() {
        return rok;
    }

    public void setRok(Integer rok) {
        this.rok = rok;
    }

    public String getPrzebieg() {
        return przebieg;
    }

    public void setPrzebieg(String przebieg) {
        this.przebieg = przebieg;
    }

    public Integer getCena() {
        return cena;
    }

    public void setCena(Integer cena) {
        this.cena = cena;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getNr_oc() {
        return nr_oc;
    }

    public void setNr_oc(String nr_oc) {
        this.nr_oc = nr_oc;
    }

    public String getNr_dowodu_rej() {
        return nr_dowodu_rej;
    }

    public void setNr_dowodu_rej(String nr_dowodu_rej) {
        this.nr_dowodu_rej = nr_dowodu_rej;
    }

    public String getPaliwo() {
        return paliwo;
    }

    public void setPaliwo(String paliwo) {
        this.paliwo = paliwo;
    }

    public String getMoc() {
        return moc;
    }

    public void setMoc(String moc) {
        this.moc = moc;
    }

    public String getSkrzynia_biegow() {
        return skrzynia_biegow;
    }

    public void setSkrzynia_biegow(String skrzynia_biegow) {
        this.skrzynia_biegow = skrzynia_biegow;
    }
}
