Projekt końcowy kursu w Software Development Academy. 
WebService wyświetla widok Administratora komisu samochodowego.

Możliwości:

 * logowanie;
 * dodawanie pojazdu do komisu z zapisem do bazy danych;
 * przeglądanie pojazdów wystawionych do sprzedaży, sprzedanych;
 * ograniczenie sprzedaży pojazdu poniżej 5k pln;
 * brak możliwości wstawienia ponownie pojazdu do komisu;

<<Wersja Rozwojowa>>